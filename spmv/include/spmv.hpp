#include <iostream>
#include <string>


#include <ginkgo/ginkgo.hpp>


#include "include/cuda/types.hpp"
#include "include/types.hpp"


enum class kernel_strategy {
    single_threaded,
    row_parallel,
    block_parallel,
    ginkgo_classical,
    cusparse
};


template <typename ValueType, typename IndexType>
void spmv(std::shared_ptr<const gko::CudaExecutor> exec,
          kernel_strategy strategy, const size_type num_rows,
          const size_type num_cols, const size_type num_nnz,
          const IndexType *row_ptrs, const IndexType *col_idxs,
          const ValueType *values, const ValueType *b, ValueType *x);
