
#include <chrono>
#include <iostream>
#include <memory>
#include <random>

#include <gflags/gflags.h>
#include <ginkgo/ginkgo.hpp>

#include "include/spmv.hpp"

#include "test/matrix_generator.hpp"

DEFINE_uint32(nrhs, 1, "The number of right hand sides");
DEFINE_int32(num_rows, -1, "The number of rows in the matrix");
DEFINE_uint32(max_nnz, 4,
              "The maximum number of non-zeros per row in the matrix");
DEFINE_string(matrix, "none", "Name of the matrix file");
DEFINE_string(strategy, "block_parallel", "Strategy for SpMV kernel");
DEFINE_uint32(num_reps, 1, "Number of repetitions");


template <typename T1, typename T2>
class SpmvFixture {
public:
    using ValueType = T1;
    using IndexType = T2;
    using DenseMat = gko::matrix::Dense<ValueType>;
    using CsrMat = gko::matrix::Csr<ValueType, IndexType>;

    SpmvFixture()
        : ref(gko::ReferenceExecutor::create()),
          cuda(gko::CudaExecutor::create(0, ref))
    {
        if (FLAGS_num_rows > 0) {
            A = gko::share(gen_mtx<CsrMat>(cuda, FLAGS_max_nnz, FLAGS_num_rows,
                                           FLAGS_num_rows));
        } else if (FLAGS_matrix.compare("none") != 0) {
            A = gko::share(
                gko::read<CsrMat>(std::ifstream(FLAGS_matrix), cuda));
        } else {
            throw std::runtime_error(
                "Need to specify num_rows or matrix file.");
        }
        b = gko::share(
            DenseMat::create(cuda, gko::dim<2>(A->get_size()[0], FLAGS_nrhs)));
        b->fill(gko::one<ValueType>());
    }

    template <typename MatType>
    std::unique_ptr<MatType> gen_mtx(std::shared_ptr<const gko::Executor> exec,
                                     size_type max_nnz_per_row,
                                     size_type num_rows, size_type num_cols)
    {
        std::ranlux48 engine(42);
        return gko::test::generate_random_matrix<MatType>(
            num_rows, num_cols,
            std::uniform_int_distribution<>(1, max_nnz_per_row),
            std::normal_distribution<>(-1.0, 1.0), engine, exec);
    }

    std::shared_ptr<gko::Executor> ref;
    std::shared_ptr<const gko::CudaExecutor> cuda;
    std::shared_ptr<CsrMat> A;
    std::shared_ptr<DenseMat> b;
};

std::chrono::time_point<std::chrono::steady_clock> tic()
{
    return std::chrono::steady_clock::now();
}

double toc(std::chrono::time_point<std::chrono::steady_clock> start)
{
    auto stop = std::chrono::steady_clock::now();
    std::chrono::duration<double> duration_time = stop - start;
    return duration_time.count();
}

int main(int argc, char *argv[])
{
    using ValueType = double;
    using IndexType = int;
    using DenseMat = gko::matrix::Dense<ValueType>;
    using CsrMat = gko::matrix::Csr<ValueType, IndexType>;
    std::ostringstream doc;
    doc << "Usage: ";
    gflags::SetUsageMessage(doc.str());
    std::ostringstream ver;
    ver << gko::version_info::get();
    gflags::SetVersionString(ver.str());
    gflags::ParseCommandLineFlags(&argc, &argv, true);
    auto strat = kernel_strategy::row_parallel;
    if (FLAGS_strategy.compare("single_threaded") == 0) {
        strat = kernel_strategy::single_threaded;
    } else if (FLAGS_strategy.compare("row_parallel") == 0) {
        strat = kernel_strategy::row_parallel;
    } else if (FLAGS_strategy.compare("block_parallel") == 0) {
        strat = kernel_strategy::block_parallel;
    } else if (FLAGS_strategy.compare("ginkgo_classical") == 0) {
        strat = kernel_strategy::ginkgo_classical;
    } else if (FLAGS_strategy.compare("cusparse") == 0) {
        strat = kernel_strategy::cusparse;
    } else {
        throw "Strategy not supported";
    }

    auto init_start = tic();
    auto fixt = SpmvFixture<ValueType, IndexType>();
    fixt.cuda->synchronize();
    auto init_time = toc(init_start);
    auto dA = CsrMat::create(fixt.cuda);
    auto db = DenseMat::create(fixt.cuda);
    auto dx = DenseMat::create(fixt.cuda, fixt.b->get_size());
    dA->copy_from(fixt.A.get());
    db->copy_from(fixt.b.get());
    fixt.cuda->synchronize();
    auto copy_time = toc(init_start);

    for (auto warm = 0; warm < 2; ++warm) {
        spmv(fixt.cuda, strat, dA->get_size()[0], dA->get_size()[1],
             dA->get_num_stored_elements(), dA->get_const_row_ptrs(),
             dA->get_const_col_idxs(), dA->get_const_values(),
             db->get_const_values(), dx->get_values());
        fixt.cuda->synchronize();
    }
    auto warm_time = toc(init_start);

    double apply_time = 0.0;
    for (auto reps = 0; reps < FLAGS_num_reps; ++reps) {
        auto apply_start = tic();
        spmv(fixt.cuda, strat, dA->get_size()[0], dA->get_size()[1],
             dA->get_num_stored_elements(), dA->get_const_row_ptrs(),
             dA->get_const_col_idxs(), dA->get_const_values(),
             db->get_const_values(), dx->get_values());
        fixt.cuda->synchronize();
        apply_time += toc(apply_start);
    }
    auto total_time = toc(init_start);
    std::string mat_name;
    if (FLAGS_matrix.compare("none") == 0) {
        mat_name = "Randomly generated";
    } else {
        mat_name = FLAGS_matrix;
    }
    std::cout << "\nMatrix: " << mat_name << "\n Size: (" << dA->get_size()[0]
              << "," << dA->get_size()[1]
              << "), non-zeros: " << dA->get_num_stored_elements()
              << "\n Strategy: " << FLAGS_strategy << "\n"
              << "\nTotal time: " << total_time << "\n Init time: " << init_time
              << "\n Copy_time: " << copy_time - init_time
              << "\n Warm time: " << warm_time - copy_time
              << "\n Apply time: " << apply_time / FLAGS_num_reps << std::endl;
}
