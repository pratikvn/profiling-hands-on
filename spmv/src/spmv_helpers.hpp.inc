namespace kernel {


template <typename IndexType = size_type>
__device__ __forceinline__ IndexType get_thread_id_flat()
{
    return threadIdx.x + static_cast<IndexType>(blockDim.x) * blockIdx.x;
}


template <typename ValueType, typename IndexType>
__global__ __launch_bounds__(default_block_size) void single_thread_spmv_kernel(
    const size_type num_rows, const size_type num_cols,
    const IndexType *__restrict__ row_ptrs,
    const IndexType *__restrict__ col_idxs,
    const ValueType *__restrict__ values, const ValueType *__restrict__ b,
    ValueType *__restrict__ x)
{
    auto global_id = get_thread_id_flat();
    if (global_id == 0) {
        for (size_type row = 0; row < num_rows; row++) {
            ValueType tmp = 0.0;
            for (auto col = row_ptrs[row]; col < row_ptrs[row + 1]; ++col) {
                tmp += values[col] * b[col_idxs[col]];
            }
            x[row] = tmp;
        }
    }
}


template <typename ValueType, typename IndexType>
__global__ __launch_bounds__(default_block_size) void row_parallel_spmv_kernel(
    const size_type num_rows, const size_type num_cols,
    const IndexType *__restrict__ row_ptrs,
    const IndexType *__restrict__ col_idxs,
    const ValueType *__restrict__ values, const ValueType *__restrict__ b,
    ValueType *__restrict__ x)
{
    auto global_id = get_thread_id_flat();
    if (global_id < num_rows) {
        ValueType tmp = 0.0;
        for (auto col = row_ptrs[global_id]; col < row_ptrs[global_id + 1];
             ++col) {
            tmp += values[col] * b[col_idxs[col]];
        }
        x[global_id] = tmp;
    }
}


template <typename ValueType, typename IndexType>
__global__
    __launch_bounds__(default_block_size) void block_parallel_spmv_kernel(
        const size_type num_rows, const size_type num_cols,
        const IndexType *__restrict__ row_ptrs,
        const IndexType *__restrict__ col_idxs,
        const ValueType *__restrict__ values, const ValueType *__restrict__ b,
        ValueType *__restrict__ x)
{
    const int block_id = blockIdx.x;
    const int nt = blockDim.x;
    const int local_id = threadIdx.x;

    ValueType tmp = 0.0;

    if (block_id < num_rows) {
        for (auto col = row_ptrs[block_id] + local_id;
             col < row_ptrs[block_id + 1]; col += nt) {
            tmp += values[col] * b[col_idxs[col]];
        }
    }

    extern __shared__ unsigned char my_tmp_shared[];
    ValueType *tmp_res = reinterpret_cast<ValueType *>(my_tmp_shared);

    tmp_res[local_id] = tmp;

    for (int k = nt / 2; k >= warp_size; k /= 2) {
        __syncthreads();
        if (local_id < k) {
            tmp_res[local_id] += tmp_res[local_id + k];
        }
    }

    tmp = tmp_res[local_id];
#pragma unroll
    for (int k = warp_size / 2; k > 0; k /= 2) {
        tmp += __shfl_xor_sync(0xffffffff, tmp, k);
    }

    if (local_id == 0) {
        x[block_id] = tmp;
    }
}


template <size_type subwarp_size, typename ValueType, typename IndexType,
          typename Closure>
__device__ void device_classical_spmv(const size_type num_rows,
                                      const size_type num_cols,
                                      const ValueType *__restrict__ val,
                                      const IndexType *__restrict__ col_idxs,
                                      const IndexType *__restrict__ row_ptrs,
                                      const ValueType *__restrict__ b,
                                      ValueType *__restrict__ c, Closure scale)
{
    auto subwarp_tile =
        gko::kernels::cuda::group::tiled_partition<subwarp_size>(
            gko::kernels::cuda::group::this_thread_block());
    const auto subrow =
        gko::kernels::cuda::thread::get_subwarp_num_flat<subwarp_size>();
    const auto subid = subwarp_tile.thread_rank();
    const auto column_id = blockIdx.y;
    auto row = gko::kernels::cuda::thread::get_subwarp_id_flat<subwarp_size>();
    for (; row < num_rows; row += subrow) {
        const auto ind_end = row_ptrs[row + 1];
        ValueType temp_val = gko::zero<ValueType>();
        for (auto ind = row_ptrs[row] + subid; ind < ind_end;
             ind += subwarp_size) {
            temp_val += val[ind] * b[col_idxs[ind] + column_id];
        }
        auto subwarp_result = gko::kernels::cuda::reduce(
            subwarp_tile, temp_val,
            [](const ValueType &a, const ValueType &b) { return a + b; });
        if (subid == 0) {
            c[row + column_id] = scale(subwarp_result, c[row + column_id]);
        }
    }
}


template <size_type subwarp_size, typename ValueType, typename IndexType>
__global__ __launch_bounds__(spmv_block_size) void abstract_classical_spmv(
    const size_type num_rows, const size_type num_cols,
    const ValueType *__restrict__ val, const IndexType *__restrict__ col_idxs,
    const IndexType *__restrict__ row_ptrs, const ValueType *__restrict__ b,
    ValueType *__restrict__ c)
{
    device_classical_spmv<subwarp_size>(
        num_rows, num_cols, val, col_idxs, row_ptrs, b, c,
        [](const ValueType &x, const ValueType &y) { return x; });
}


}  // namespace kernel
