#include <iostream>
#include <string>
#include <type_traits>


#include "cuda_runtime.h"
#include "cusparse.h"

#include "../include/spmv.hpp"


constexpr int default_block_size = 512;
constexpr int warp_size = 32;
constexpr int warps_in_block = 4;


#include "include/cuda/config.hpp"


namespace gko {
namespace kernels {
namespace cuda {
namespace thread {

#include "include/cuda/thread_ids.hpp.inc"

}
}  // namespace cuda
}  // namespace kernels
}  // namespace gko


#include "include/cuda/cooperative_groups.cuh"
#include "include/cuda/cusparse_bindings.hpp"
#include "include/cuda/pointer_mode_guard.hpp"
#include "include/cuda/uninitialized_array.hpp.inc"

namespace gko {
namespace kernels {
namespace cuda {

#include "include/cuda/reduction.hpp.inc"


}  // namespace cuda
}  // namespace kernels
}  // namespace gko


constexpr int spmv_block_size =
    warps_in_block * gko::kernels::cuda::config::warp_size;
constexpr int wsize = gko::kernels::cuda::config::warp_size;
constexpr int classical_overweight = 32;
constexpr int subwarp_size = 4;


#include "spmv_helpers.hpp.inc"


template <typename ValueType, typename IndexType>
void spmv(std::shared_ptr<const gko::CudaExecutor> exec,
          kernel_strategy strategy, const size_type num_rows,
          const size_type num_cols, const size_type num_nnz,
          const IndexType *row_ptrs, const IndexType *col_idxs,
          const ValueType *values, const ValueType *b, ValueType *x)
{
    if (strategy == kernel_strategy::single_threaded) {
        kernel::single_thread_spmv_kernel<<<num_rows, default_block_size>>>(
            num_rows, num_cols, row_ptrs, col_idxs,
            gko::kernels::cuda::as_cuda_type(values),
            gko::kernels::cuda::as_cuda_type(b),
            gko::kernels::cuda::as_cuda_type(x));
    } else if (strategy == kernel_strategy::ginkgo_classical) {
        const auto nwarps = exec->get_num_warps_per_sm() *
                            exec->get_num_multiprocessor() *
                            classical_overweight;
        const auto gridx =
            std::min(gko::ceildiv(num_rows, spmv_block_size / subwarp_size),
                     static_cast<gko::int64>(nwarps / warps_in_block));
        const dim3 grid(gridx, 1);
        const dim3 block(spmv_block_size);
        kernel::abstract_classical_spmv<subwarp_size><<<grid, block, 0, 0>>>(
            num_rows, num_cols, gko::kernels::cuda::as_cuda_type(values),
            col_idxs, row_ptrs, gko::kernels::cuda::as_cuda_type(b),
            gko::kernels::cuda::as_cuda_type(x));
    } else if (strategy == kernel_strategy::cusparse) {
        auto handle = exec->get_cusparse_handle();
        {
            gko::kernels::cuda::cusparse::pointer_mode_guard pm_guard(handle);
            auto descr = gko::kernels::cuda::cusparse::create_mat_descr();
            const auto alpha = gko::one<ValueType>();
            const auto beta = gko::zero<ValueType>();
            gko::kernels::cuda::cusparse::spmv(
                handle, CUSPARSE_OPERATION_NON_TRANSPOSE, num_rows, num_cols,
                num_nnz, &alpha, descr, values, row_ptrs, col_idxs, b, &beta,
                x);
            gko::kernels::cuda::cusparse::destroy(descr);
        }
    } else if (strategy == kernel_strategy::row_parallel) {
        kernel::row_parallel_spmv_kernel<<<num_rows, default_block_size>>>(
            num_rows, num_cols, row_ptrs, col_idxs,
            gko::kernels::cuda::as_cuda_type(values),
            gko::kernels::cuda::as_cuda_type(b),
            gko::kernels::cuda::as_cuda_type(x));

    } else if (strategy == kernel_strategy::block_parallel) {
        dim3 block(default_block_size);
        kernel::block_parallel_spmv_kernel<<<num_rows, block,
                                             block.x * sizeof(ValueType)>>>(
            num_rows, num_cols, row_ptrs, col_idxs,
            gko::kernels::cuda::as_cuda_type(values),
            gko::kernels::cuda::as_cuda_type(b),
            gko::kernels::cuda::as_cuda_type(x));
    } else {
        throw "Unsupported strategy";
    }
}


#define NLA4HPC_DECLARE_SPMV(ValueType, IndexType)                  \
    void spmv(std::shared_ptr<const gko::CudaExecutor> exec,        \
              kernel_strategy strategy, const size_type num_rows,   \
              const size_type num_cols, const size_type num_nnz,    \
              const IndexType *row_ptrs, const IndexType *col_idxs, \
              const ValueType *values, const ValueType *b, ValueType *x)

NLA4HPC_INSTANTIATE_FOR_EACH_NON_COMPLEX_VALUE_AND_INDEX_TYPE(
    NLA4HPC_DECLARE_SPMV);
