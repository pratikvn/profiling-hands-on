
#include <gtest/gtest.h>


#include <ginkgo/ginkgo.hpp>


#include <fstream>
#include <iostream>
#include <string>


#include "test/utils.hpp"


#include "../include/spmv.hpp"


template <typename ValueIndexType>
class CsrSpmv : public ::testing::Test {
protected:
    using ValueType =
        typename std::tuple_element<0, decltype(ValueIndexType())>::type;
    using IndexType =
        typename std::tuple_element<1, decltype(ValueIndexType())>::type;
    using DenseMat = gko::matrix::Dense<ValueType>;
    using CsrMat = gko::matrix::Csr<ValueType, IndexType>;

    CsrSpmv()
        : ref(gko::ReferenceExecutor::create()),
          cuda(gko::CudaExecutor::create(0, ref))
    {
        gko_A = gen_mtx<CsrMat>(ref, 4, 313, 313);
        gko_b = gen_mtx<DenseMat>(ref, 4, 313, 1);
        gko_x = gen_mtx<DenseMat>(ref, 4, 313, 1);
    }

    template <typename MatType>
    std::unique_ptr<MatType> gen_mtx(std::shared_ptr<const gko::Executor> exec,
                                     size_type max_nnz_per_row,
                                     size_type num_rows, size_type num_cols)
    {
        std::ranlux48 engine(42);
        return gko::test::generate_random_matrix<MatType>(
            num_rows, num_cols,
            std::uniform_int_distribution<>(1, max_nnz_per_row),
            std::normal_distribution<>(-1.0, 1.0), engine, exec);
    }

    std::shared_ptr<gko::Executor> ref;
    std::shared_ptr<const gko::CudaExecutor> cuda;
    std::unique_ptr<CsrMat> gko_A;
    std::unique_ptr<DenseMat> gko_b;
    std::unique_ptr<DenseMat> gko_x;
};


TYPED_TEST_SUITE(CsrSpmv, gko::test::RealValueIndexTypes);


TYPED_TEST(CsrSpmv, SingleThreadedKernelIsCorrect)
{
    using ValueType = typename TestFixture::ValueType;
    using IndexType = typename TestFixture::IndexType;
    using DenseMat = gko::matrix::Dense<ValueType>;
    using CsrMat = gko::matrix::Csr<ValueType, IndexType>;

    auto dA = CsrMat::create(this->cuda);
    auto db = DenseMat::create(this->cuda);
    auto dx = DenseMat::create(this->cuda);
    dA->copy_from(this->gko_A.get());
    db->copy_from(this->gko_b.get());
    dx->copy_from(this->gko_x.get());

    this->gko_A->apply(this->gko_b.get(), this->gko_x.get());

    // The function that you should implement
    spmv(this->cuda, kernel_strategy::single_threaded, dA->get_size()[0],
         dA->get_size()[1], dA->get_num_stored_elements(),
         dA->get_const_row_ptrs(), dA->get_const_col_idxs(),
         dA->get_const_values(), db->get_const_values(), dx->get_values());

    auto x_host = DenseMat::create(this->ref);
    x_host->copy_from(dx.get());

    GKO_ASSERT_MTX_NEAR(x_host.get(), this->gko_x.get(), r<ValueType>::value);
}


TYPED_TEST(CsrSpmv, RowParallelKernelIsCorrect)
{
    using ValueType = typename TestFixture::ValueType;
    using IndexType = typename TestFixture::IndexType;
    using DenseMat = gko::matrix::Dense<ValueType>;
    using CsrMat = gko::matrix::Csr<ValueType, IndexType>;

    auto dA = CsrMat::create(this->cuda);
    auto db = DenseMat::create(this->cuda);
    auto dx = DenseMat::create(this->cuda);
    dA->copy_from(this->gko_A.get());
    db->copy_from(this->gko_b.get());
    dx->copy_from(this->gko_x.get());

    this->gko_A->apply(this->gko_b.get(), this->gko_x.get());

    // The function that you should implement
    spmv(this->cuda, kernel_strategy::row_parallel, dA->get_size()[0],
         dA->get_size()[1], dA->get_num_stored_elements(),
         dA->get_const_row_ptrs(), dA->get_const_col_idxs(),
         dA->get_const_values(), db->get_const_values(), dx->get_values());

    auto x_host = DenseMat::create(this->ref);
    x_host->copy_from(dx.get());

    GKO_ASSERT_MTX_NEAR(x_host.get(), this->gko_x.get(), r<ValueType>::value);
}


TYPED_TEST(CsrSpmv, BlockParallelKernelIsCorrect)
{
    using ValueType = typename TestFixture::ValueType;
    using IndexType = typename TestFixture::IndexType;
    using DenseMat = gko::matrix::Dense<ValueType>;
    using CsrMat = gko::matrix::Csr<ValueType, IndexType>;

    auto dA = CsrMat::create(this->cuda);
    auto db = DenseMat::create(this->cuda);
    auto dx = DenseMat::create(this->cuda);
    dA->copy_from(this->gko_A.get());
    db->copy_from(this->gko_b.get());
    dx->copy_from(this->gko_x.get());

    this->gko_A->apply(this->gko_b.get(), this->gko_x.get());

    // The function that you should implement
    spmv(this->cuda, kernel_strategy::block_parallel, dA->get_size()[0],
         dA->get_size()[1], dA->get_num_stored_elements(),
         dA->get_const_row_ptrs(), dA->get_const_col_idxs(),
         dA->get_const_values(), db->get_const_values(), dx->get_values());

    auto x_host = DenseMat::create(this->ref);
    x_host->copy_from(dx.get());

    GKO_ASSERT_MTX_NEAR(x_host.get(), this->gko_x.get(), r<ValueType>::value);
}


TYPED_TEST(CsrSpmv, GinkgoClassicalKernelIsCorrect)
{
    using ValueType = typename TestFixture::ValueType;
    using IndexType = typename TestFixture::IndexType;
    using DenseMat = gko::matrix::Dense<ValueType>;
    using CsrMat = gko::matrix::Csr<ValueType, IndexType>;

    auto dA = CsrMat::create(this->cuda);
    auto db = DenseMat::create(this->cuda);
    auto dx = DenseMat::create(this->cuda);
    dA->copy_from(this->gko_A.get());
    db->copy_from(this->gko_b.get());
    dx->copy_from(this->gko_x.get());

    this->gko_A->apply(this->gko_b.get(), this->gko_x.get());

    // The function that you should implement
    spmv(this->cuda, kernel_strategy::ginkgo_classical, dA->get_size()[0],
         dA->get_size()[1], dA->get_num_stored_elements(),
         dA->get_const_row_ptrs(), dA->get_const_col_idxs(),
         dA->get_const_values(), db->get_const_values(), dx->get_values());

    auto x_host = DenseMat::create(this->ref);
    x_host->copy_from(dx.get());

    GKO_ASSERT_MTX_NEAR(x_host.get(), this->gko_x.get(), r<ValueType>::value);
}


TYPED_TEST(CsrSpmv, CusparseKernelIsCorrect)
{
    using ValueType = typename TestFixture::ValueType;
    using IndexType = typename TestFixture::IndexType;
    using DenseMat = gko::matrix::Dense<ValueType>;
    using CsrMat = gko::matrix::Csr<ValueType, IndexType>;

    auto dA = CsrMat::create(this->cuda);
    auto db = DenseMat::create(this->cuda);
    auto dx = DenseMat::create(this->cuda);
    dA->copy_from(this->gko_A.get());
    db->copy_from(this->gko_b.get());
    dx->copy_from(this->gko_x.get());

    this->gko_A->apply(this->gko_b.get(), this->gko_x.get());

    // The function that you should implement
    spmv(this->cuda, kernel_strategy::cusparse, dA->get_size()[0],
         dA->get_size()[1], dA->get_num_stored_elements(),
         dA->get_const_row_ptrs(), dA->get_const_col_idxs(),
         dA->get_const_values(), db->get_const_values(), dx->get_values());

    auto x_host = DenseMat::create(this->ref);
    x_host->copy_from(dx.get());

    GKO_ASSERT_MTX_NEAR(x_host.get(), this->gko_x.get(), r<ValueType>::value);
}
