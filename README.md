## Profiling with CUDA, hands-on

### Prerequisites

1. We make use of Ginkgo's features and we recommend installing Ginkgo externally and storing the installation path in `Ginkgo_DIR` environment variable. The framework should then automatically pick up the externally installed Ginkgo.

### Compiling the framework.

Compile in the usual cmake fashion.

``` sh
mkdir build && cd build && cmake ..
```

### Running the driver

We focus on the SpMV and a driver for that can be created in the `spmv/spmv` executable. Please see the `spmv/spmv.cpp` file for the parameters available. We use `gflags` to ease the command line argument parsing.

With the driver, you can either:

1. Run SpMV with randomly generated matrices (slower, because the matrix generation is slower for larger matrices, but more control)
2. Run SpMV for matrices by reading them from files.

Some of the important options that you can specify to the executable are:

1. `--matrix="./path/to/.mtx/file"`: The path to the matrix market file to profile.
2. `--strategy="STRATEGY"`: The SpMV algorithm, which can be one of: `block_parallel`, `row_parallel`, `cusparse`, `ginkgo_classical`, `single_threaded`.
3. `--num_reps=N`: The number of repetitions to remove the effects of noise. A default warm iterations of 2 is performed in addition to this number.


### Profiling

There are multiple tools available to profile CUDA code. Here we focus on [NVIDIA NSight Compute](https://docs.nvidia.com/nsight-compute/NsightCompute/index.html) to profile the individual kernels.

To profile and trace larger applications, with possibly multiple GPUs NVIDIA recommends [NVIDIA NSight Systems](https://docs.nvidia.com/nsight-systems/UserGuide/index.html).

Nsight compute gives you various metrics to enable for your profiling run. For the complete list see the [Nsight Compute User Guide](https://docs.nvidia.com/nsight-compute/ProfilingGuide/index.html#metric-collection).
Some of the important ones are `ComputeWorkloadAnalysis`, `MemoryWorkloadAnalysis`, `SpeedofLight` and `Occupancy`. 

